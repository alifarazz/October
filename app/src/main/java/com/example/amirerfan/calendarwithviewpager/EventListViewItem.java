package com.example.amirerfan.calendarwithviewpager;

/**
 * Created by Amirerfan on 1/26/2018.
 */

public class EventListViewItem {

    private String title;
    private String content;
    private String location;

    //constructor
    public EventListViewItem(String title, String content, String location) {
        this.title = title;
        this.content = content;
        this.location = location;
    }


    //getter and setter


    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
