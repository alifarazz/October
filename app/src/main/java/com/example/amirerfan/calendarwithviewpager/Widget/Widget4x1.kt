package com.example.amirerfan.calendarwithviewpager.Widget


//import com.example.amirerfan.calendarwithviewpager.Widget.Utils.shape
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.ComponentName
import android.content.Context
import android.util.Log
import android.util.SparseArray
import android.widget.RemoteViews
import com.example.amirerfan.calendarwithviewpager.DayEvent.Events
import com.example.amirerfan.calendarwithviewpager.JalaliCalendar
import com.example.amirerfan.calendarwithviewpager.R
import java.util.*
import android.content.Intent
import android.content.IntentFilter
import android.content.BroadcastReceiver



/**
 * Created by luce on 1/26/18.
 */

class Widget4x1 : AppWidgetProvider() {

    companion object {
        var receiver: BroadcastReceiver? = null
        var previousAppWidgetManager: AppWidgetManager? = null
    }

    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {
        super.onUpdate(context, appWidgetManager, appWidgetIds)

        previousAppWidgetManager = appWidgetManager

        appWidgetIds.forEach { _ ->
            val view = RemoteViews(context.packageName, R.layout.widget_4x1)
            val thisWidget = ComponentName(context, Widget4x1::class.java)
            val today = JalaliCalendar()
            val todayCalendar = Calendar.getInstance()

            val meridiemToString = { am: Int ->
                if (am == 1)
                    "ب.ظ"
                else
                    "ق.ظ"
            }

            val timeAdjuster = {time:String ->
                if (time.length < 2)
                    '٠' + time
                else
                    time
            }

            view.setTextViewText(R.id.textPlaceholder1_4x1, "${timeAdjuster(convertToPersian(todayCalendar.get(Calendar.HOUR)))}:" +
                    "${timeAdjuster(convertToPersian(todayCalendar.get(Calendar.MINUTE)))} " +
                    "${meridiemToString(todayCalendar.get(Calendar.AM_PM))}")

            view.setTextViewText(R.id.textPlaceholder2_4x1, "${today.dayOfWeekString} " +
                    "${convertToPersian(today.day)} ${today.monthString} ${convertToPersian(today.year)} \n"
                    + Events.getDailyEvents(JalaliCalendar())
            )

            Log.i("Widget", "onCreate called")

            appWidgetManager.updateAppWidget(thisWidget, view)
        }
    }

    override fun onEnabled(context: Context?) {
        super.onEnabled(context)

        receiver = object : BroadcastReceiver() {
            override fun onReceive(ctx: Context, intent: Intent) {
                if (intent.action!!.compareTo(Intent.ACTION_TIME_TICK) == 0) {
                    // update widget time here using System.currentTimeMillis()
                    val view = RemoteViews(context?.packageName, R.layout.widget_4x1)
                    val thisWidget = ComponentName(context, Widget4x1::class.java)
                    val todayCalendar = Calendar.getInstance()
                    val today = JalaliCalendar()

                    val meridiemToString = { a: Int ->
                        if (a == 1)
                            "ب.ظ"
                        else
                            "ق.ظ"
                    }
                    val timeAdjuster = {time:String ->
                        if (time.length < 2)
                            '٠' + time
                        else
                            time
                    }

                    view.setTextViewText(R.id.textPlaceholder1_4x1, "${timeAdjuster(convertToPersian(todayCalendar.get(Calendar.HOUR)))}:" +
                            "${timeAdjuster(convertToPersian(todayCalendar.get(Calendar.MINUTE)))} " +
                            "${meridiemToString(todayCalendar.get(Calendar.AM_PM))}")

                    view.setTextViewText(R.id.textPlaceholder2_4x1, "${today.dayOfWeekString} " +
                            "${convertToPersian(today.day)} ${today.monthString} ${convertToPersian(today.year)} \n"
                            + Events.getDailyEvents(JalaliCalendar())
                    )
                    previousAppWidgetManager?.updateAppWidget(thisWidget, view)

                    Log.i("widget", "receiver works")
                }
            }
        }
        context!!.applicationContext!!.registerReceiver(receiver, IntentFilter(Intent.ACTION_TIME_TICK))
    }

    override fun onDisabled(context: Context?) {
        super.onDisabled(context)
        if (receiver != null) {
            context?.applicationContext?.unregisterReceiver(receiver)
            Log.i("widget", "reciver destroy")

        }

    }
    //    companion object {
//        private fun setRemoteAdapterV11(context: Context, views: RemoteViews) {
//            views.setRemoteAdapter(0, R.id.widget_layout4x1,
//                    Intent(context, WidgetService::class.java))
//        }
//        internal fun updateAppView(context: Context, appWidgetManager: AppWidgetManager,
//                                   appWidgetId: Int) {
//            val views = RemoteViews(context.packageName, R.layout.widget_4x1)
//            //        Intent intent = new Intent(context, )
//
//            setRemoteAdapterV11(context, views)
//
//            appWidgetManager.updateAppWidget(appWidgetId, views)
//        }// set up widget


    //
    //    @Override
    //    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
    //        super.onUpdate(context, appWidgetManager, appWidgetIds);
    //    }

    internal object Utils {
        fun getDayOfWeek(jalaliCalendar: JalaliCalendar): Int {
            return jalaliCalendar.toGregorian().get(Calendar.DAY_OF_WEEK)
        }

        fun updateTime() {

        }
    }
}
