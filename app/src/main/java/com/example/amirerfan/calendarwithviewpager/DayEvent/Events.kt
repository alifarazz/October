package com.example.amirerfan.calendarwithviewpager.DayEvent

import android.content.Context
import android.util.Log
import com.example.amirerfan.calendarwithviewpager.JalaliCalendar

import java.util.ArrayList

/**
 * Created by luce on 1/27/18.
 */

class Events {

    var title: String? = null
    var isEveryYear: Boolean = false
    var date: JalaliCalendar? = null
    var isHoliday: Boolean = false



    companion object {
        var eventsArrayList = ArrayList<Events>()

        fun getDailyEvents(today: JalaliCalendar?): String? {
//        return JsonEventList.set()
            Log.i("Widget", eventsArrayList.size.toString())
            for (event in eventsArrayList)
                if (event.date?.day == today?.day && event.date?.month == today?.month && (event.isEveryYear || event.date?.year == today?.year))
                    return event.title
            return  ""
        }
    }


}

