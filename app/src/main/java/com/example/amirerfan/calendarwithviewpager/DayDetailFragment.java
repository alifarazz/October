package com.example.amirerfan.calendarwithviewpager;


import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by Amirerfan on 12/22/2017.
 */

public class DayDetailFragment extends Fragment {

    public String[] eventLocations = {"eram garden", "Azadi park", "Molasadra street", "Mofateh dormitory"};
    ArrayList<EventListViewItem> events = new ArrayList<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.activity_day_detail_fragment, container, false);

        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "BNAZANB.TTF");
        Typeface numtf = Typeface.createFromAsset(getActivity().getAssets(), "BNAZANIN.TTF");
        JalaliCalendar jalaliCalendar = new JalaliCalendar();

        int day = jalaliCalendar.getDay();

        TextView todayDate = view.findViewById(R.id.currentDay);
        todayDate.setTypeface(numtf);

        todayDate.setText(String.valueOf(day));

        String dayOfWeek = jalaliCalendar.getDayOfWeekString();
        TextView dayOfWeekTextView = view.findViewById(R.id.currentDayOfWeek);
        dayOfWeekTextView.setText(dayOfWeek);
        dayOfWeekTextView.setTypeface(tf);

        String month = jalaliCalendar.getMonthString();
        TextView currentMonth = view.findViewById(R.id.currentMonth);
        currentMonth.setText(month);
        currentMonth.setTypeface(tf);


        //Generate List View of Day Tasks
        //get Stuff

        //listview adaptor
        ListView dayTasks = view.findViewById(R.id.dayEventListView);
        ListAdapter dayTaskListViewAdapter = new CustomeAdapter();
        dayTasks.setAdapter(dayTaskListViewAdapter);

        return view;
    }

    public class CustomeAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return events.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = getLayoutInflater().inflate(R.layout.activity_month_event_listview, null);

            TextView title = convertView.findViewById(R.id.event_subject);
            TextView content = convertView.findViewById(R.id.event_content);

            title.setText(events.get(position).getTitle());
            content.setText(events.get(position).getContent());

            return convertView;
        }
    }
}


