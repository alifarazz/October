package com.example.amirerfan.calendarwithviewpager;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.util.GregorianCalendar;

/**
 * Created by Amirerfan on 12/22/2017.
 */

public class ConverterFragment extends Fragment {

    Button convert;
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        final View view = inflater.inflate(R.layout.activity_convertor_fragment, container, false);

        convert = view.findViewById(R.id.convertButton);
        convert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                try {


                    EditText shamsi_year = getActivity().findViewById(R.id.shamsi_year);
                    EditText shamsi_month = getActivity().findViewById(R.id.shamsi_month);
                    EditText shamsi_day = getActivity().findViewById(R.id.shamsi_day);

                    JalaliCalendar shamsi2miladi = new JalaliCalendar();
                    shamsi2miladi.set(Integer.parseInt(shamsi_year.getText().toString()),
                            Integer.parseInt(shamsi_month.getText().toString()),
                            Integer.parseInt(shamsi_day.getText().toString()));

                    TextView shamsi2miladiText = getActivity().findViewById(R.id.shamsi_to_miladi_text);
                    shamsi2miladiText.setText(shamsi2miladi.getDayOfWeekString() + "  " + String.valueOf(shamsi2miladi.toGregorian().getTime().getDate())
                            + " / " + String.valueOf(shamsi2miladi.toGregorian().getTime().getMonth() + 1) + " / " + String.valueOf(shamsi2miladi.toGregorian().getTime().getYear() + 1900));
                } catch (Exception ee) {
                    TextView shamsi2miladiText = getActivity().findViewById(R.id.shamsi_to_miladi_text);
                    shamsi2miladiText.setText("");
                }
                try {
                    EditText miladi_day = getActivity().findViewById(R.id.miladi_day);
                    EditText miladi_month = getActivity().findViewById(R.id.miladi_month);
                    EditText miladi_year = getActivity().findViewById(R.id.miladi_year);

                    GregorianCalendar miladi2shamsi = new GregorianCalendar(Integer.parseInt(miladi_year.getText().toString()),
                            Integer.parseInt(miladi_month.getText().toString()) - 1,
                            Integer.parseInt(miladi_day.getText().toString()));

                    JalaliCalendar miladiConverted = new JalaliCalendar();
                    miladiConverted.fromGregorian(miladi2shamsi);
                    TextView miladi2shamsiText = getActivity().findViewById(R.id.miladi_to_shamsi_text);
                    miladi2shamsiText.setText(miladiConverted.getDayOfWeekString() + "  " + String.valueOf(miladiConverted.getDay())
                            + " / " + String.valueOf(miladiConverted.getMonth()) + " / " +
                            String.valueOf(miladiConverted.getYear()));
                } catch (Exception e) {
                    TextView miladi2shamsiText = getActivity().findViewById(R.id.miladi_to_shamsi_text);
                    miladi2shamsiText.setText("");
                }
            }
        });
        return view;
    }
}
