package com.example.amirerfan.calendarwithviewpager;

import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;


/**
 * Created by Amirerfan on 12/24/2017.
 */


public class YearNameFragment extends Fragment {

    public static TextView year_text;
    public static View fragmentView;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.activity_year_name, container, false);
        setYearString(view);
        fragmentView = view;

        Button prevButton = view.findViewById(R.id.prev_year);
        Button nextButton = view.findViewById(R.id.next_year);

//        prevButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                if (yearViewPager.getCurrentItem() > 0) {
//                    yearViewPager.setCurrentItem(yearViewPager.getCurrentItem() + 1);
////                };
//                showing_year++;
//                TextView yearString = getActivity().findViewById(R.id.year_string);
//                yearString.setText(String.valueOf(showing_year));
//            }
//        });
//
//        nextButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
////                if (yearViewPager.getCurrentItem() < 12) {
//                    yearViewPager.setCurrentItem(yearViewPager.getCurrentItem() - 1);
////                }
//                showing_year--;
//                TextView yearString = getActivity().findViewById(R.id.year_string);
//                yearString.setText(String.valueOf(showing_year));
//
//            }
//        });


        return view;
    }

    public void setYearString(View view) {
        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "BNAZANIN.TTF");
        year_text = view.findViewById(R.id.year_string);
        year_text.setTypeface(tf);
        year_text.setText(String.valueOf((MonthDetailFragment.showing_year)));
    }

}
