package com.example.amirerfan.calendarwithviewpager;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.amirerfan.calendarwithviewpager.MonthFragments.MonthTableFragmentAban;
import com.example.amirerfan.calendarwithviewpager.MonthFragments.MonthTableFragmentAzar;
import com.example.amirerfan.calendarwithviewpager.MonthFragments.MonthTableFragmentBahman;
import com.example.amirerfan.calendarwithviewpager.MonthFragments.MonthTableFragmentDey;
import com.example.amirerfan.calendarwithviewpager.MonthFragments.MonthTableFragmentEsfand;
import com.example.amirerfan.calendarwithviewpager.MonthFragments.MonthTableFragmentFarvardin;
import com.example.amirerfan.calendarwithviewpager.MonthFragments.MonthTableFragmentKhordad;
import com.example.amirerfan.calendarwithviewpager.MonthFragments.MonthTableFragmentMehr;
import com.example.amirerfan.calendarwithviewpager.MonthFragments.MonthTableFragmentMordad;
import com.example.amirerfan.calendarwithviewpager.MonthFragments.MonthTableFragmentOrdibehesht;
import com.example.amirerfan.calendarwithviewpager.MonthFragments.MonthTableFragmentShahrivar;
import com.example.amirerfan.calendarwithviewpager.MonthFragments.MonthTableFragmentTir;

import java.util.ArrayList;
import java.util.Calendar;

import static com.example.amirerfan.calendarwithviewpager.YearNameFragment.year_text;

/**
 * Created by Amirerfan on 12/22/2017.
 */

public class MonthDetailFragment extends Fragment {
    public static int current_month_position;
    public static int current_year_position = 150;
    public static int showing_year;
    public static JalaliCalendar clicked_day = new JalaliCalendar();
    public static TextView rooydadText;
    public static ArrayList<EventListViewItem> clickedDayEvents = new ArrayList<>();
    public static ListView clickedDayListView;
    public static CustomeAdapter clickedDayAdapter;
    public static SeekBar seekBar;
    public static ViewPager monthTableViewPager;
    public static ViewPager yearViewPager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @SuppressLint("ClickableViewAccessibility")
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        final View view = inflater.inflate(R.layout.activity_month_detail_fragment, container, false);


        JalaliCalendar jalaliCalendar = new JalaliCalendar();

        final int currentYear = jalaliCalendar.getYear();
        int currentMonth = jalaliCalendar.getMonth();
        int currentDay = jalaliCalendar.getDay();

        clicked_day.set(currentYear, currentMonth, currentDay);
        current_month_position = 12 - currentMonth;
        showing_year = currentYear;

        yearViewPager = view.findViewById(R.id.viewpager_year);
        final CustomePagerAdapter customePagerAdapter = new CustomePagerAdapter(getActivity().getSupportFragmentManager());
        yearViewPager.setAdapter(customePagerAdapter);

        yearViewPager.setCurrentItem(150);

        yearViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                Log.i("pos", String.valueOf(position));
                Log.i("year", String.valueOf(showing_year));
                showing_year = showing_year - (position - current_year_position);
                current_year_position = current_year_position + (position - current_year_position);

                year_text.setText(String.valueOf(showing_year));

                CustomePagerAdapterMonthTable customePagerAdapterMonthTableNew = new CustomePagerAdapterMonthTable(getActivity().getSupportFragmentManager());
                ViewPager viewPager = getActivity().findViewById(R.id.month_table_viewpager);
                viewPager.setAdapter(customePagerAdapterMonthTableNew);
                viewPager.setCurrentItem(current_month_position);
                TextView yearString = view.findViewById(R.id.year_string);
                yearString.setText(String.valueOf(showing_year));
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        monthTableViewPager = view.findViewById(R.id.month_table_viewpager);
        CustomePagerAdapterMonthTable customePagerAdapterMonthTable = new CustomePagerAdapterMonthTable(getActivity().getSupportFragmentManager());
        monthTableViewPager.setAdapter(customePagerAdapterMonthTable);

        monthTableViewPager.setCurrentItem(12 - currentMonth);

        monthTableViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                seekBar.setProgress(position);
                current_month_position = position;
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        //popup listener part
        final FloatingActionButton popup = view.findViewById(R.id.popupButton);
        popup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // TODO:// call android calendar api and create a new event
                // Snackbar.make(view, "TODO:// Add calender event", Snackbar.LENGTH_LONG).setAction("Action", null).show();
//                Snackbar.make(view, "هولو", Snackbar.LENGTH_LONG).setAction("Action", null).show();
                Intent eventIntent = new Intent(Intent.ACTION_INSERT)
                        .setType("vnd.android.cursor.item/event")
                        .putExtra("beginTime", Calendar.getInstance());
                Log.i("+/eventIntent", eventIntent.toString());
                startActivity(eventIntent);
                Log.i("+/eventIntent", "ended");
            }
        });

        setRooydad(view);

        //fill list view part
        //data
        //Adapter
        clickedDayListView = view.findViewById(R.id.month_events_listview);
        clickedDayAdapter = new CustomeAdapter();
        clickedDayListView.setAdapter(clickedDayAdapter);


        // seek bar
        seekBar = view.findViewById(R.id.seekBar);
        seekBar.setProgress(12 - (currentMonth));
        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                if (b)
                    monthTableViewPager.setCurrentItem(i);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });


//        clickedDayListView.setOnScrollListener(new AbsListView.OnScrollListener() {
//            int mLastFirstVisibleItem;
//
//            @Override
//            public void onScrollStateChanged(AbsListView view, int scrollState) {
//
//            }
//
//            @Override
//            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
//                if (mLastFirstVisibleItem < firstVisibleItem) {
//                    FloatingActionButton floatingActionButton = getActivity().findViewById(R.id.popupButton);
//                    floatingActionButton.setSize(FloatingActionButton.SIZE_MINI);
//
//                }
//                if (mLastFirstVisibleItem > firstVisibleItem) {
//                    FloatingActionButton floatingActionButton = getActivity().findViewById(R.id.popupButton);
//                    floatingActionButton.setSize(FloatingActionButton.SIZE_NORMAL);
//                }
//                mLastFirstVisibleItem = firstVisibleItem;
//            }
//        });

        return view;
    }

    private void setRooydad(View view) {
        Typeface tf = Typeface.createFromAsset(getActivity().getAssets(), "BNAZANIN.TTF");
        rooydadText = view.findViewById(R.id.eventsDay);
        rooydadText.setText("   رویدادهای " + String.valueOf(clicked_day.getDayOfWeekString() + " " + clicked_day.getDay()) + "  " + clicked_day.getMonthString() + " " + String.valueOf(clicked_day.getYear()));
        rooydadText.setTypeface(tf);
    }

    public class CustomePagerAdapter extends FragmentPagerAdapter {
        public CustomePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return 300;
        }

        @Override
        public Fragment getItem(int position) {
            return new YearNameFragment();
        }
    }

    public class CustomePagerAdapterMonthTable extends FragmentPagerAdapter {
        public CustomePagerAdapterMonthTable(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return 12;
        }

        @Override
        public Fragment getItem(int position) {

            switch (position) {
                case 11:
                    return new MonthTableFragmentFarvardin();
                case 10:
                    return new MonthTableFragmentOrdibehesht();
                case 9:
                    return new MonthTableFragmentKhordad();
                case 8:
                    return new MonthTableFragmentTir();
                case 7:
                    return new MonthTableFragmentMordad();
                case 6:
                    return new MonthTableFragmentShahrivar();
                case 5:
                    return new MonthTableFragmentMehr();
                case 4:
                    return new MonthTableFragmentAban();
                case 3:
                    return new MonthTableFragmentAzar();
                case 2:
                    return new MonthTableFragmentDey();
                case 1:
                    return new MonthTableFragmentBahman();
                case 0:
                    return new MonthTableFragmentEsfand();
            }
            return null;
        }
    }

    public class CustomeAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return clickedDayEvents.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = getLayoutInflater().inflate(R.layout.activity_month_event_listview, null);

            TextView title = convertView.findViewById(R.id.event_subject);
            TextView content = convertView.findViewById(R.id.event_content);

            title.setText(clickedDayEvents.get(position).getTitle());
            content.setText(clickedDayEvents.get(position).getContent());

            return convertView;
        }
    }


}
