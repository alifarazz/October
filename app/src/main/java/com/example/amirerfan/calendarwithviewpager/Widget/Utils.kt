package com.example.amirerfan.calendarwithviewpager.Widget

import android.util.Log
import java.util.*
import kotlin.collections.HashMap

/**
 * Created by luce on 1/27/18.
 */

fun convertToPersian(intText: String): String{
    val arabicChars = charArrayOf('٠', '١', '٢', '٣', '٤', '٥', '٦', '٧', '٨', '٩')
    var arabicNumber = ""
    for (letter in intText.toCharArray()) {
        Log.i("convert", letter.toString())
        arabicNumber += arabicChars[letter.toInt() - '0'.toInt()]
        Log.i("convert", letter.toString() + "add")
    }
    return arabicNumber
}

fun convertToPersian(intText: Int): String {
    return convertToPersian(intText.toString())
}