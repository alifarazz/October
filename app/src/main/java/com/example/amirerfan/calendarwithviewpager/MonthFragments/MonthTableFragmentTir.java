package com.example.amirerfan.calendarwithviewpager.MonthFragments;

import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.example.amirerfan.calendarwithviewpager.JalaliCalendar;
import com.example.amirerfan.calendarwithviewpager.MonthDetailFragment;
import com.example.amirerfan.calendarwithviewpager.R;

import java.util.ArrayList;

import static com.example.amirerfan.calendarwithviewpager.MonthDetailFragment.clicked_day;

/**
 * Created by Amirerfan on 12/28/2017.
 */

public class MonthTableFragmentTir extends Fragment {
    public Button clicked;
    public Typeface tf;
    public int month = 4;
    public Button todayButton;
    public JalaliCalendar jalaliCalendar;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);
        View view = inflater.inflate(R.layout.activity_month_table, container, false);

        JalaliCalendar jalaliCalendar = new JalaliCalendar((MonthDetailFragment.showing_year), 4, 1);


        TextView monthName = view.findViewById(R.id.month_name);
        monthName.setText("تیر");

        int currentDay = 40;

        JalaliCalendar currentDate = new JalaliCalendar();
        if (currentDate.getMonth() == 4) {
            currentDay = currentDate.getDay();
        }


        ArrayList<Button> days = new ArrayList<Button>();
        days.add((Button) view.findViewById(R.id.day1));
        days.add((Button) view.findViewById(R.id.day2));
        days.add((Button) view.findViewById(R.id.day3));
        days.add((Button) view.findViewById(R.id.day4));
        days.add((Button) view.findViewById(R.id.day5));
        days.add((Button) view.findViewById(R.id.day6));
        days.add((Button) view.findViewById(R.id.day7));
        days.add((Button) view.findViewById(R.id.day8));
        days.add((Button) view.findViewById(R.id.day9));
        days.add((Button) view.findViewById(R.id.day10));
        days.add((Button) view.findViewById(R.id.day11));
        days.add((Button) view.findViewById(R.id.day12));
        days.add((Button) view.findViewById(R.id.day13));
        days.add((Button) view.findViewById(R.id.day14));
        days.add((Button) view.findViewById(R.id.day15));
        days.add((Button) view.findViewById(R.id.day16));
        days.add((Button) view.findViewById(R.id.day17));
        days.add((Button) view.findViewById(R.id.day18));
        days.add((Button) view.findViewById(R.id.day19));
        days.add((Button) view.findViewById(R.id.day20));
        days.add((Button) view.findViewById(R.id.day21));
        days.add((Button) view.findViewById(R.id.day22));
        days.add((Button) view.findViewById(R.id.day23));
        days.add((Button) view.findViewById(R.id.day24));
        days.add((Button) view.findViewById(R.id.day25));
        days.add((Button) view.findViewById(R.id.day26));
        days.add((Button) view.findViewById(R.id.day27));
        days.add((Button) view.findViewById(R.id.day28));
        days.add((Button) view.findViewById(R.id.day29));
        days.add((Button) view.findViewById(R.id.day30));
        days.add((Button) view.findViewById(R.id.day31));
        days.add((Button) view.findViewById(R.id.day32));
        days.add((Button) view.findViewById(R.id.day33));
        days.add((Button) view.findViewById(R.id.day34));
        days.add((Button) view.findViewById(R.id.day35));
        days.add((Button) view.findViewById(R.id.day36));
        days.add((Button) view.findViewById(R.id.day37));

        int dayOfWeek = jalaliCalendar.getDayOfWeek();
        int length = jalaliCalendar.getxLength();
        dayOfWeek = dayOfWeek % 7;

        int i = 0;
        int j = 1;
        for (; i < dayOfWeek; i++) {
            Button myday = days.get(i);
            myday.setVisibility(View.INVISIBLE);
        }

        for (; j < length + 1; i++, j++) {
            final Button myday = days.get(i);
            myday.setText(String.valueOf(j));
            tf = Typeface.createFromAsset(getActivity().getAssets(), "BNAZANB.TTF");
            myday.setTypeface(tf);
            myday.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
                @Override
                public void onClick(View v) {
                    dayClicked(myday);
                }
            });
            if (j == currentDay & (MonthDetailFragment.showing_year) == jalaliCalendar.getYear()) {
                myday.setBackgroundResource(R.drawable.blackround);
            }
        }

        for (; i < 37; i++) {
            Button myday = days.get(i);
            myday.setVisibility(View.INVISIBLE);
        }


        return view;
    }

    @RequiresApi(api = Build.VERSION_CODES.N)
    private void dayClicked(Button day) {
        JalaliCalendar jalaliCalendar = new JalaliCalendar();

        JalaliCalendar jal = new JalaliCalendar();
        jal.setDay(Integer.parseInt(day.getText().toString()));
        jal.setMonth(4);
        Utils.updateDayEvents(getContext(), jal);

        if (clicked != null)
            if (clicked == todayButton)
                clicked.setBackgroundResource(R.drawable.blackround);
            else
                clicked.setBackgroundResource(R.drawable.empty);
        day.setBackgroundResource(R.drawable.redround);
        clicked = day;
        clicked_day.set(MonthDetailFragment.showing_year, month, Integer.parseInt((String) clicked.getText()));
        MonthDetailFragment.rooydadText.setText("   رویدادهای  " + String.valueOf(clicked_day.getDayOfWeekString() + " " + clicked_day.getDay()) + "  " + clicked_day.getMonthString() + " " + String.valueOf(clicked_day.getYear()));
    }
}
