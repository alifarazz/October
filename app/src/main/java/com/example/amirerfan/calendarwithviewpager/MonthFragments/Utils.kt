@file:JvmName("Utils")

package com.example.amirerfan.calendarwithviewpager.MonthFragments

import android.Manifest
import android.content.Context
import android.content.ContextWrapper
import android.content.pm.PackageManager
import android.database.Cursor
import android.os.Build
import android.provider.CalendarContract
import android.support.annotation.RequiresApi
import android.support.v4.content.ContextCompat
import android.util.Log
import com.example.amirerfan.calendarwithviewpager.EventListViewItem
import com.example.amirerfan.calendarwithviewpager.JalaliCalendar
import com.example.amirerfan.calendarwithviewpager.MonthDetailFragment
import com.example.amirerfan.calendarwithviewpager.MonthDetailFragment.clickedDayEvents
import com.example.amirerfan.calendarwithviewpager.Widget.convertToPersian
import java.util.*


/**
 * Created by luce on 1/27/18.
 */


// todo: populate a listView of Events and show them, set onclicklisten for event edit
// todo : long press for delete
internal object data {
    val TAG = "Utils"
}

@RequiresApi(Build.VERSION_CODES.N)
fun updateDayEvents(context: Context, todayJal: JalaliCalendar) {

    clickedDayEvents.clear()

    val projection = arrayOf(CalendarContract.Events.CALENDAR_ID, CalendarContract.Events.TITLE, CalendarContract.Events.DESCRIPTION, CalendarContract.Events.DTSTART, CalendarContract.Events.DTEND, CalendarContract.Events.ALL_DAY, CalendarContract.Events.EVENT_LOCATION)
// 0 = January, 1 = February, ...

    val startTime = todayJal.toGregorian()
//    startTime.add(Calendar.DATE, -1)
    startTime.set(GregorianCalendar.HOUR, 0)
    startTime.set(GregorianCalendar.MINUTE, 0)

    val endTime = todayJal.toGregorian()
//    startTime.add(Calendar.DATE, -1)
    endTime.set(GregorianCalendar.HOUR, 23)
    endTime.set(GregorianCalendar.MINUTE, 59)


//
    val selection = "(( " + CalendarContract.Events.DTSTART + " >= " + startTime.timeInMillis + " ) AND ( " + CalendarContract.Events.DTSTART + " <= " + endTime.timeInMillis + " ))"

    val cursor: Cursor? = run {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.READ_CALENDAR) == PackageManager.PERMISSION_GRANTED)
            ContextWrapper(context).baseContext.contentResolver.query(CalendarContract.Events.CONTENT_URI, projection, selection, null, null)
        else
            null
    }
//    Log.i(data.TAG, "cursor is null (${cursor==null})")
    val meridiemToString = { am: Int ->
        if (am == 1)
            "ب.ظ"
        else
            "ق.ظ"
    }

    val timeAdjuster = { time: String ->
        if (time.length < 2)
            '٠' + time
        else
            time
    }
    try {
        if (cursor!!.moveToFirst()) {
            do {
//                Toast.makeText(context, "Title: " + cursor.getString(1) + " Start-Time: " + Date(cursor.getLong(3)).toString(), Toast.LENGTH_LONG).show()
                var gre: GregorianCalendar? = null
                val jal = JalaliCalendar(run {
                    gre = GregorianCalendar()
                    gre?.time = Date(cursor.getLong(3))
                    gre
                })
                clickedDayEvents.add(EventListViewItem(cursor.getString(1), "" +
                        "${timeAdjuster(convertToPersian(gre!!.get(GregorianCalendar.HOUR)))}:" +
                        "${timeAdjuster(convertToPersian(gre!!.get(GregorianCalendar.MINUTE)))} " +
                        "${meridiemToString(gre!!.get((GregorianCalendar.AM_PM)))}", ""))
            } while (cursor.moveToNext())
        }
    } catch (e: Exception) {
        Log.i(data.TAG, e.message)
    }

    MonthDetailFragment.clickedDayAdapter.notifyDataSetChanged()
}
