package com.example.amirerfan.calendarwithviewpager

import android.Manifest
import android.annotation.TargetApi
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.support.annotation.RequiresApi
import android.support.design.widget.Snackbar
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.example.amirerfan.calendarwithviewpager.DayEvent.JsonEventList
import kotlinx.android.synthetic.main.activity_main.*


//import com.example.amirerfan.calendarwithviewpager.Widget.Utils;

class MainActivity : AppCompatActivity() {

    protected var handler = Handler()
    @TargetApi(Build.VERSION_CODES.M)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val viewPager = main_viewpager
        val customePagerAdapter = CustomePagerAdapter(supportFragmentManager)
        viewPager.adapter = customePagerAdapter

        JsonEventList.set()

        requestCalenderPermission()
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun requestCalenderPermission() {
        Log.i(TAG, "calender permission not granted, requesting ...")
        if (shouldShowRequestPermissionRationale(Manifest.permission.READ_CALENDAR)) {
            Log.i(TAG, "displaying calender info rationale for additional context")
            Snackbar.make(main_viewpager, R.string.permissions_calender_rationale, Snackbar.LENGTH_INDEFINITE)
                    .setAction(R.string.ok, {
                        requestPermissions(PERMISSION_CALENDER, REQUEST_READ_CALENDER)
                    }).show()
        } else
            requestPermissions(PERMISSION_CALENDER, REQUEST_READ_CALENDER)
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        //                super(requestCode, permissions, grantResults)
        when (requestCode) {
            REQUEST_READ_CALENDER,
            REQUEST_WRITE_CALENDER -> {
                if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                    Snackbar.make(main_viewpager, R.string.permission_available_calender,
                            Snackbar.LENGTH_SHORT).show()
                else
                    Snackbar.make(main_viewpager, R.string.permissions_not_granted_calender,
                            Snackbar.LENGTH_SHORT).show()
            }
            else -> {
            }

        }
    }
    inner class CustomePagerAdapter(fm: FragmentManager) : FragmentPagerAdapter(fm) {

        override fun getCount(): Int {
            return 3
        }

        override fun getItem(position: Int): Fragment? {
            if (position == 0) {
                return DayDetailFragment()
            }
            if (position == 1) {
                return MonthDetailFragment()
            }
            return if (position == 2) {
                ConverterFragment()
            } else
                null
        }
    }


    companion object {
        const val TAG = "MainActivity"
        const val REQUEST_READ_CALENDER = 10
        const val REQUEST_WRITE_CALENDER = 11
        val PERMISSION_CALENDER = arrayOf(Manifest.permission.READ_CALENDAR, Manifest.permission.WRITE_CALENDAR)
    }
}
